#!/bin/sh
set -e

DEB_VERSION=`dpkg-parsechangelog | awk '/^Version: / { print $2 }'`
if [ "$GDC" = "" ]; then
        GDC=gdc
fi

# link against libcurl
LIBS=`pkg-config --libs libcurl 2>/dev/null || echo "-lcurl"`

echo "Generating version file for $DEB_VERSION..."
echo "module dub.version_;" > source/dub/version_.d
echo "enum dubVersion = \"$DEB_VERSION\";" >> source/dub/version_.d

echo "Running $GDC..."
set -x
$GDC -Wall -obin/dub -fversion=DubUseCurl -Isource \
	-Wl,--push-state,--no-as-needed $LIBS -lz -Wl,--pop-state \
	$CFLAGS \
	$LDFLAGS \
	$* @build-files.txt
set +x
echo "DUB has been built as bin/dub."
